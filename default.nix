{ pkgs ? import <nixpkgs> { } }:
with pkgs;
stdenv.mkDerivation {
  name = "cv";
  src = ./src;
  buildInputs = [
    texlive.combined.scheme-full
  ];
  buildPhase = ''
    latexmk -xelatex -pdf cv.tex
    latexmk -xelatex -pdf coverletter.tex
    '';
  installPhase = ''
    mkdir -p $out
    cp cv.pdf $out/
    cp coverletter.pdf $out/
    '';
}
