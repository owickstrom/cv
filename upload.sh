#!/usr/bin/env bash

result=$(nix-build --no-out-link)
aws s3 cp ${result}/cv.pdf s3://wickstrom.tech/cv.pdf
